package me.jorgemoran.cc_mitm_framework.common.activity

import android.content.Context
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.webkit.WebView
import android.widget.Button
import android.widget.EditText
import android.widget.Switch
import android.widget.Toast
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionDeniedResponse
import com.karumi.dexter.listener.PermissionGrantedResponse
import com.karumi.dexter.listener.single.PermissionListener
import kotlinx.android.synthetic.main.activity_main.*
import me.jorgemoran.cc_mitm_framework.common.R
import me.jorgemoran.cc_mitm_framework.common.entity.MESSAGE_TYPE
import me.jorgemoran.cc_mitm_framework.common.entity.Message
import me.jorgemoran.cc_mitm_framework.common.hardware.HardwareHelper
import me.jorgemoran.cc_mitm_framework.common.hardware.PermissionsHelper
import me.jorgemoran.cc_mitm_framework.common.logging.LoggingHelper
import me.jorgemoran.cc_mitm_framework.common.network.WebSocketHelper
import me.jorgemoran.cc_mitm_framework.common.network.WebSocketListener
import me.jorgemoran.cc_mitm_framework.common.persistence.PreferencesHelper
import java.io.PrintWriter
import java.io.StringWriter

abstract class BaseActivity : AppCompatActivity(), WebSocketListener {
    protected var websocketHelper: WebSocketHelper? = null
    protected var connectionsText: String? = null
    protected var listening = false
    protected lateinit var connectionLogWv: WebView
    protected lateinit var listeningToggleButton: Button
    protected lateinit var serverAddressEditText: EditText
    abstract protected var currentBehavior: Int
    val hwHelper = HardwareHelper(this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        connectionsText = "Connections log"
        if (!hwHelper.hasNfcAndIsEnabled()) {
            Toast.makeText(this, R.string.no_nfc, Toast.LENGTH_LONG).show()
            finish();
        }

    }

    fun initUI() {
        val wsHost = PreferencesHelper.getWebSocketHost(baseContext)
        val saveLogs = PreferencesHelper.getSaveLogToFile(baseContext)
        val useSecureWebsockets = PreferencesHelper.getUseSecureWebsocket(baseContext)
        toggleLogging.isChecked = saveLogs
        toggleSecureWS.isChecked = useSecureWebsockets
        if (!wsHost.isEmpty()) {
            serverAddressEditText.setText(wsHost)
        }
    }

    override fun onStart() {
        super.onStart()
        initUI()
    }

    override fun onStop() {
        super.onStop()
        closeConnectionClearLog();

    }

    fun closeConnection() {
        websocketHelper?.close(WebSocketHelper.CONNECTION_CLOSE_OK);
        listening = false;
    }

    fun closeConnectionClearLog() {
        closeConnection();
        connectionsText = getString(R.string.connection_log_title)
    }

    fun resetActivity() {

        runOnUiThread(object : Runnable {
            override fun run() {
                listening = false
                closeConnectionClearLog()
                connectionLogWv.loadData(connectionsText, "text/html; charset=utf-8", "utf-8")
                listeningToggleButton.setText(R.string.start_listening)
            }
        })
        PreferencesHelper.clearTransactionalValues(baseContext)
    }

    override fun connectionClosed(reason: String?) {
        resetActivity()
        if (reason != null) {
            runOnUiThread(object : Runnable {
                override fun run() {
                    if (!reason.isNullOrEmpty()) {
                        Toast.makeText(baseContext, reason, Toast.LENGTH_LONG).show()
                    }
                }
            })
        }
    }

    fun startListening(view: View) {

        val host = serverAddressEditText.text.toString()
        try {
            var baseUrl = ""
            if (PreferencesHelper.getUseSecureWebsocket(baseContext)) {
                baseUrl = getString(R.string.secure_websocket_full_path)
            } else {
                baseUrl = getString(R.string.websocket_full_path)
            }
            val wsPath = baseUrl.format(host)
            if (!listening) {
                websocketHelper = WebSocketHelper(wsPath, this, currentBehavior)
                listeningToggleButton.setText(R.string.stop_listening)
                listening = true;
            } else {
                listeningToggleButton.setText(R.string.start_listening)
                closeConnectionClearLog()
            }
            PreferencesHelper.setWebSocketHost(baseContext, host)
        } catch (e: Exception) {
            serverAddressEditText.setText("")
            LoggingHelper.writeExceptionToErrorLog(baseContext, e)
            Toast.makeText(baseContext, e.toString(), Toast.LENGTH_LONG).show()
        }
    }

    override fun connectionFailed(t: Throwable?) {
        resetActivity()
        val sw = StringWriter()
        val pw = PrintWriter(sw)
        t?.printStackTrace(pw)
        val sStackTrace = sw.toString() // stack trace as a string
        LoggingHelper.writeToFile(baseContext, "Error that caused disconnection: ${sStackTrace
                ?: ""}", LoggingHelper.LOG_TYPE_ERROR)
        runOnUiThread(object : Runnable {
            override fun run() {

                Toast.makeText(this@BaseActivity, R.string.connection_failed, Toast.LENGTH_LONG).show()

            }
        })
        resetActivity()

    }

    abstract fun handleTxMessage(msg: String?)
    abstract fun handleAuthMessage(msg: String?)

    override fun messageReceived(msg: String?) {
        LoggingHelper.writeToFile(baseContext, "Message received from socket:\n $msg", LoggingHelper.LOG_TYPE_VERBOSE)
        var responseMsg = Message.MessageFromJson(msg ?: "")

        if (responseMsg.type == MESSAGE_TYPE.AUTH.type) {
            PreferencesHelper.setTransactionUUID(baseContext, responseMsg.uuid?:"")
            connectionsText = "${baseContext.getString(R.string.connected_string)} <br> $connectionsText"
            handleAuthMessage(msg)
        } else if (responseMsg.type == MESSAGE_TYPE.TX.type) {


            handleTxMessage(msg)
        } else if (responseMsg.type == MESSAGE_TYPE.NOTIFICATION.type) {
            connectionsText = "$connectionsText <br/> ${responseMsg.apduRequest}"
        }
        if (responseMsg.type != MESSAGE_TYPE.NOTIFICATION.type) {

            if (responseMsg.uuid != null && responseMsg.uuid!!.isNotEmpty()) {
                connectionsText = "$connectionsText <hr><br/>"
                connectionsText = "$connectionsText ${baseContext.getString(R.string.title_transaction_id)}"
                connectionsText = "$connectionsText <div style=\"width:100%;word-break: break-all; word-wrap: break-word;\">${responseMsg.uuid}</div> <br/>"
            }
            if (responseMsg.apduRequest != null && responseMsg.apduRequest!!.isNotEmpty()) {
                connectionsText = "$connectionsText ${baseContext.getString(R.string.title_apdu_command)}"
                connectionsText = "$connectionsText <div style=\"width:100%;word-break: break-all; word-wrap: break-word;\">${responseMsg.apduRequest}</div> <br/>"
            }

            if (responseMsg.apduResponse != null && responseMsg.apduResponse!!.isNotEmpty()) {
                connectionsText = "$connectionsText ${baseContext.getString(R.string.title_apdu_response)}"
                connectionsText = "$connectionsText <div style=\"width:100%;word-break: break-all; word-wrap: break-word;\">${responseMsg.apduResponse}</div> <br/>"
            }
        }
        runOnUiThread(object : Runnable {
            override fun run() {
                connectionLogWv.loadData(connectionsText, "text/html; charset=utf-8", "utf-8")
                mainScrollview.fullScroll(View.FOCUS_DOWN)
            }
        })
    }

    fun toggleLoggingAction(view: View) {
        val switch = view as Switch
        if (switch.isChecked) {
            PreferencesHelper.setSaveLogToFile(baseContext, true)
            PermissionsHelper.requestWriteToFilesPermission(this, object : PermissionListener {
                override fun onPermissionGranted(response: PermissionGrantedResponse?) {

                }

                override fun onPermissionRationaleShouldBeShown(permission: com.karumi.dexter.listener.PermissionRequest?, token: PermissionToken?) {

                }

                override fun onPermissionDenied(response: PermissionDeniedResponse?) {
                    switch.isChecked = false
                    Toast.makeText(baseContext, R.string.write_log_permission_not_grantes, Toast.LENGTH_LONG).show()
                    PreferencesHelper.setSaveLogToFile(baseContext, false)
                }
            })
        } else {
            PreferencesHelper.setSaveLogToFile(baseContext, false)
        }
    }

    fun toggleSecureWebsocketAction(view: View) {
        val switch: Switch = view as Switch
        PreferencesHelper.setUseSecureWebsocket(baseContext, switch.isChecked)

    }

    override fun getContextForWebsocket(): Context {
        return baseContext
    }
}