package me.jorgemoran.cc_mitm_framework.common.apdu

import java.util.*


class MessagesHelper{
    companion object {
        val SELECT_OK_SW = HexStringToByteArray("9000");
        val PPSE_AID = "325041592E5359532E4444463031"
        val SELECT_APDU_HEADER = "00A40400";

        fun statusWordFromMessage(message: ByteArray):ByteArray{
            val resultLength = message.size
            val statusWord = byteArrayOf(message[resultLength - 2], message[resultLength - 1])
            return statusWord;
        }
        fun payloadFromMessage(message: ByteArray):ByteArray{

            return Arrays.copyOf(message, message.size - 2)
        }

        fun HexStringToByteArray(mString : String?) : ByteArray{
            val length = mString?.length?:0;
            val i: Int
            if(length % 2 ==1){
                throw  kotlin.IllegalArgumentException("Hex string must have even number of characters");
            }
            val data = ByteArray(length/2)
            val tmpStr = mString?:""
            for( i in 0..(length-1) step 2){

                data[i/2] = ((Character.digit(tmpStr.elementAt(i),16) shl 4) + Character.digit(tmpStr.elementAt(i+1),16)).toByte()
            }
            return data;
        }
        fun ByteArrayToHexString(mByteArray: ByteArray? ) : String{
            var hexArray : CharArray = charArrayOf('0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F');
            var hexChars = CharArray((mByteArray?.size?:0)*2)
            var v : Int
            val tmpByteArray=mByteArray?:ByteArray(1)
            for( j in 0..((mByteArray?.size?:0)-1)){

                v =  tmpByteArray.elementAt(j).toInt() and 0xFF;
                hexChars.set(j*2, hexArray.elementAt(v ushr 4))
                hexChars.set(j*2 +1 , hexArray.elementAt(v and 0x0F))
            }
            return String(hexChars);
        }

        fun ConcatArrays(first: ByteArray, vararg rest: ByteArray): ByteArray {
            var totalLength = first.size
            for (array in rest) {
                totalLength += array.size
            }
            val result = Arrays.copyOf(first, totalLength)
            var offset = first.size
            for (array in rest) {
                System.arraycopy(array, 0, result, offset, array.size)
                offset += array.size
            }
            return result
        }

        fun BuildSelectApdu(aid: String): ByteArray {
            // Format: [CLASS | INSTRUCTION | PARAMETER 1 | PARAMETER 2 | LENGTH | DATA]
            return HexStringToByteArray(SELECT_APDU_HEADER + String.format("%02X", aid.length / 2) + aid)
        }

    }
}