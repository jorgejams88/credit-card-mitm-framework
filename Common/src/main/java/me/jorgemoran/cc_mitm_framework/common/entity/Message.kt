package me.jorgemoran.cc_mitm_framework.common.entity

import com.google.gson.Gson

class Message(var origin: Int,var type: Int) {

    var apduRequest: String? = null
    var apduResponse: String? = null
    var uuid: String? = null

    constructor(origin: Int, type: Int, uuid: String, apduRequest: String, apduResponse: String) : this(origin,type, uuid, apduRequest) {

        this.apduResponse = apduResponse

    }

    constructor(origin: Int, type: Int, apduRequest: String) : this(origin, type) {
        this.apduRequest = apduRequest
    }

    constructor(origin: Int, type: Int, uuid: String, apduRequest: String) : this(origin,type) {

        this.apduRequest = apduRequest
        this.uuid = uuid

    }

    fun toJson(): String {
        val gson = Gson()
        return gson.toJson(this)
    }

    companion object {
        fun MessageFromJson(json: String): Message {
            val gson = Gson()
            return gson.fromJson(json, Message::class.java)
        }
    }
}

enum class MESSAGE_ORIGIN(val type: Int) {
    CARD(0),
    READER(1)

}

enum class MESSAGE_TYPE(val type: Int) {
    AUTH(0),
    TX(1),
    NOTIFICATION(3)

}
