package me.jorgemoran.cc_mitm_framework.common.hardware

import android.app.Activity
import android.content.ComponentName
import android.content.Context
import android.nfc.NfcAdapter
import android.nfc.cardemulation.CardEmulation
import android.nfc.cardemulation.HostApduService
import android.util.Log

class HardwareHelper (val ctx: Activity){
    companion object {
        val TAG="HARDWARE_HELPER"
        var READER_FLAGS =
                NfcAdapter.FLAG_READER_NFC_A or NfcAdapter.FLAG_READER_SKIP_NDEF_CHECK or NfcAdapter.FLAG_READER_NFC_B
    }
    fun hasNfcAndIsEnabled():Boolean{
        val nfcAdapter=NfcAdapter.getDefaultAdapter(ctx)
        return nfcAdapter!=null && nfcAdapter.isEnabled;
    }

     fun enableReader(cardReader: NfcAdapter.ReaderCallback) {
        Log.v(TAG, "Enabling reader mode")
        val nfc = NfcAdapter.getDefaultAdapter(ctx)
        nfc?.enableReaderMode(ctx, cardReader, READER_FLAGS, null)
    }

     fun disableReaderMode() {
        Log.v(TAG, "Disabling reader mode")
        val nfc = NfcAdapter.getDefaultAdapter(ctx)
        nfc?.disableReaderMode(ctx)
    }
    fun isAppDefaultHcePaymentsHandler(cls: Class<out HostApduService>):Boolean{

      return CardEmulation.getInstance(NfcAdapter.getDefaultAdapter(ctx)).isDefaultServiceForCategory(ComponentName(ctx, cls), CardEmulation.CATEGORY_PAYMENT)

    }
}