package me.jorgemoran.cc_mitm_framework.common.hardware

import com.karumi.dexter.Dexter
import com.karumi.dexter.listener.single.PermissionListener
import me.jorgemoran.cc_mitm_framework.common.activity.BaseActivity
import android.Manifest


class PermissionsHelper {
    companion object {
        fun requestWriteToFilesPermission(act: BaseActivity, listener: PermissionListener){
            Dexter.withActivity(act).withPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE).withListener(listener).check()
        }
    }

}