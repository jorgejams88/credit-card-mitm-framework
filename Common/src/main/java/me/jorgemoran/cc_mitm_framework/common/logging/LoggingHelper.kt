package me.jorgemoran.cc_mitm_framework.common.logging

import android.content.Context
import android.os.Environment
import android.util.Log
import me.jorgemoran.cc_mitm_framework.common.R
import me.jorgemoran.cc_mitm_framework.common.persistence.PreferencesHelper
import java.io.*
import java.security.Permission
import java.text.SimpleDateFormat
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.util.*

class LoggingHelper {

    companion object {
        val LOG_TYPE_ERROR = 2
        val LOG_TYPE_INFO = 1
        val LOG_TYPE_VERBOSE = 3
        val TAG = "CC_MITM_CARD_LOGGER"
        fun writeToFile(ctx: Context, line: String) {
            writeToFile(ctx, line, LOG_TYPE_INFO)
        }

        fun writeToFile(ctx: Context, line: String, type: Int) {
            val canWrite = PreferencesHelper.getSaveLogToFile(ctx)
            if (!canWrite) {
                return
            }
            try {
                val sdCard = Environment.getExternalStorageDirectory()
                val dir = File(sdCard.absolutePath + File.separator + ctx.getString(R.string.logs_path))

                if (!dir.exists()) {
                    dir.mkdirs()
                }
                val cal = Calendar.getInstance()
                var finalLine: String

                finalLine = "${cal.timeInMillis}\t$line"
                var fileName: String
                when (type) {
                    1 -> {
                        fileName = ctx.getString(R.string.info_log_name)
                        Log.i(TAG, finalLine)
                    }
                    2 -> {
                        fileName = ctx.getString(R.string.error_log_name)
                        Log.e(TAG, finalLine)
                    }
                    3 -> {
                        fileName = ctx.getString(R.string.verbose_log_name)
                        Log.v(TAG, finalLine)
                    }
                    else -> {
                        throw RuntimeException("Undefined log type")
                    }
                }

                val file = File(dir, fileName)
                val fout = FileOutputStream(file, true)
                val osw = OutputStreamWriter(fout)
                osw.append("$finalLine \n")
                osw.flush()
                osw.close()
            } catch (e: Exception) {
                Log.e(TAG, e.message)
            }

        }

        fun writeExceptionToErrorLog(ctx: Context, t: Throwable?) {
            val sw = StringWriter()
            val pw = PrintWriter(sw)
            t?.printStackTrace(pw)
            val sStackTrace = sw.toString() // stack trace as a string
            LoggingHelper.writeToFile(ctx, "Exception thrown: \n ${sStackTrace
                    ?: ""}", LoggingHelper.LOG_TYPE_ERROR)
            Log.wtf(TAG, sStackTrace)
        }
    }
}