package me.jorgemoran.cc_mitm_framework.common.network

import android.content.Context
import android.util.Log
import me.jorgemoran.cc_mitm_framework.common.R
import me.jorgemoran.cc_mitm_framework.common.entity.MESSAGE_TYPE
import me.jorgemoran.cc_mitm_framework.common.entity.Message
import okhttp3.*
import java.lang.ref.WeakReference
import java.util.concurrent.TimeUnit

class WebSocketHelper(var url: String, var listener: me.jorgemoran.cc_mitm_framework.common.network.WebSocketListener, var origin: Int) {
    val client: OkHttpClient = OkHttpClient.Builder().readTimeout(3, TimeUnit.SECONDS).build()
    var websocket: WebSocket?=null
    companion object {
        var CONNECTION_CLOSE_ABRUPT = 1001;
        var CONNECTION_CLOSE_OK = 1000;
        val TAG="WEBSOCKET_CONNECTION"
        var currentWebsocket: WebSocketHelper? = null
    }
    var connected: Boolean = false
        get() = connected
        private set

    init {
        currentWebsocket = this
        connect()
    }
    fun connect()
    {
        val request: Request = Request.Builder().url(url).build()
        websocket = client.newWebSocket(request, object : okhttp3.WebSocketListener() {
            override fun onMessage(webSocket: WebSocket?, text: String?) {
                Log.v("TAG", text);

                listener.messageReceived(text)
            }

            override fun onOpen(webSocket: WebSocket?, response: Response?) {
                connected = true
                var authMessage = Message(this@WebSocketHelper.origin, MESSAGE_TYPE.AUTH.type)
                webSocket?.send(authMessage.toJson())
            }

            override fun onFailure(webSocket: WebSocket?, t: Throwable?, response: Response?) {
                connected = false
                listener.connectionFailed(t)
            }

            override fun onClosed(webSocket: WebSocket?, code: Int, reason: String?) {
                connected = false
                Log.v(TAG, reason )
                listener.connectionClosed(reason)
            }

            override fun onClosing(webSocket: WebSocket?, code: Int, reason: String?) {
                super.onClosing(webSocket, code, reason)
                listener.connectionClosed(reason)
                websocket?.close(code,reason)
            }
        })
    }
    fun sendMessage(msg: String) {
        websocket?.send(msg);
    }

    fun close(type : Int)
    {
        var reason: String
        if(type == CONNECTION_CLOSE_OK)
        {
            reason = listener.getContextForWebsocket().getString(R.string.normal_connection_close)
        }
        else{
            reason = listener.getContextForWebsocket().getString(R.string.abnormal_connection_close)
        }
        websocket?.close(type,reason);
    }
}
interface WebSocketListener{
    fun messageReceived(msg: String?)
    fun getContextForWebsocket(): Context
    fun connectionClosed(reason: String?)
    fun connectionFailed(t: Throwable?)
}