package me.jorgemoran.cc_mitm_framework.common.persistence

import android.content.Context
import android.content.SharedPreferences
import me.jorgemoran.cc_mitm_framework.common.R
import me.jorgemoran.cc_mitm_framework.common.entity.Message
import me.jorgemoran.cc_mitm_framework.common.logging.LoggingHelper

class PreferencesHelper {

    companion object {
        fun storeValue(ctx: Context, key: String, value: String) {
            LoggingHelper.writeToFile(ctx, "Storing $value in $key as a preference", LoggingHelper.LOG_TYPE_VERBOSE)
            ctx.getSharedPreferences(ctx.getString(R.string.shared_preferences_file), Context.MODE_PRIVATE).edit().putString(key, value).apply()
        }

        /*fun forceStoreValue(ctx: Context, key: String, value: String) {
            LoggingHelper.writeToFile(ctx, "Storing $value in $key as a preference", LoggingHelper.LOG_TYPE_VERBOSE)
            ctx.getSharedPreferences(ctx.getString(R.string.shared_preferences_file), Context.MODE_PRIVATE).edit().putString(key, value).commit()
        }*/

        fun getValue(ctx: Context, key: String): String {
            return ctx.getSharedPreferences(ctx.getString(R.string.shared_preferences_file), Context.MODE_PRIVATE).getString(key, "")
        }

        fun setWebSocketHost(ctx: Context, ip: String) {
            storeValue(ctx, ctx.getString(R.string.websocket_ip_preference), ip)
        }

        fun getWebSocketHost(ctx: Context): String {
            return getValue(ctx, ctx.getString(R.string.websocket_ip_preference))
        }

        fun setTransactionUUID(ctx: Context, uuid: String) {
            storeValue(ctx, ctx.getString(R.string.transaction_uuid), uuid)
        }

        fun getTransactionUUID(ctx: Context): String {
            return getValue(ctx, ctx.getString(R.string.transaction_uuid))
        }

        fun setMessageCache(ctx: Context, msg: String) {
            storeValue(ctx, ctx.getString(R.string.last_message), msg)
        }

        fun getMessageCache(ctx: Context): String {
            return getValue(ctx, ctx.getString(R.string.last_message))
        }

        fun setSaveLogToFile(ctx: Context, msg: Boolean) {
            storeValue(ctx, ctx.getString(R.string.save_log_to_file), msg.toString())
        }

        fun getSaveLogToFile(ctx: Context): Boolean {
            return getValue(ctx, ctx.getString(R.string.save_log_to_file)).toBoolean()
        }

        fun setUseSecureWebsocket(ctx: Context, msg: Boolean) {
            storeValue(ctx, ctx.getString(R.string.use_secure_websocket), msg.toString())
        }

        fun getUseSecureWebsocket(ctx: Context): Boolean {
            return getValue(ctx, ctx.getString(R.string.use_secure_websocket)).toBoolean()
        }


        fun clearTransactionalValues(ctx: Context) {
            val sharedPreferences = ctx.getSharedPreferences(ctx.getString(R.string.shared_preferences_file), Context.MODE_PRIVATE)
            sharedPreferences.edit().apply {
                remove(ctx.getString(R.string.current_message))
                remove(ctx.getString(R.string.transaction_uuid))
                remove(ctx.getString(R.string.shared_preferences_file))
            }.apply()
        }
        fun clearMessageCache(ctx: Context){
            ctx.getSharedPreferences(ctx.getString(R.string.shared_preferences_file), Context.MODE_PRIVATE).edit().remove(ctx.getString(R.string.last_message)).apply()
        }

    }
}