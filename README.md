#Credit Card man-in-the-middle framework - Chasqui and Inca

##Chasqui
Application that works as an NFC reader to start a relay attack

##Inca
Using information coming from Chasqui, executes a payment on a POS

##Open source libraries that are required
- [GSON](https://github.com/google/gson)
- [OkHttp](https://github.com/square/okhttp)
- [Dexter](https://github.com/Karumi/Dexter)