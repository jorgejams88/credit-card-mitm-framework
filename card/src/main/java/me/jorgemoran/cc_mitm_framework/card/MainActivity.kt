package me.jorgemoran.cc_mitm_framework.card

import android.content.*
import android.nfc.cardemulation.CardEmulation
import android.os.Bundle
import android.support.v4.content.LocalBroadcastManager
import android.util.Log
import kotlinx.android.synthetic.main.activity_main.*
import me.jorgemoran.cc_mitm_framework.card.apdu.CreditCardService
import me.jorgemoran.cc_mitm_framework.common.activity.BaseActivity
import me.jorgemoran.cc_mitm_framework.common.entity.MESSAGE_ORIGIN
import me.jorgemoran.cc_mitm_framework.common.entity.Message
import me.jorgemoran.cc_mitm_framework.common.logging.LoggingHelper
import me.jorgemoran.cc_mitm_framework.common.persistence.PreferencesHelper
import java.util.Arrays.asList
import android.nfc.NfcAdapter
import android.content.ComponentName
import java.util.*


class MainActivity : BaseActivity() {

    //private lateinit var messageReceiver: MessageFromReaderBroadcastReceiver
    override var currentBehavior: Int
        get() = MESSAGE_ORIGIN.CARD.type
        set(value) {}

    companion object {
        val CREDITCARACTIVITY_INTENT = "me.jorgemoran.cc_mitm_framework.card.message-from-service"

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(me.jorgemoran.cc_mitm_framework.common.R.layout.activity_main)
        listeningToggleButton = btnStart
        serverAddressEditText = editSrvAddr
        connectionLogWv = connectionsLog
        if (!hwHelper.isAppDefaultHcePaymentsHandler(CreditCardService::class.java)) {
            changeTapAndPay()
        } else {
            Log.v("TAG", "App is enabled as default");
        }
        val myName = ComponentName(this, CreditCardService::class.java);
        var aidStat = false
        val adapter2 = NfcAdapter.getDefaultAdapter(this)
        val cardEmulation2 = CardEmulation.getInstance(adapter2)
        var dynamicAIDList: List<String>
        dynamicAIDList = baseContext.resources.getStringArray(me.jorgemoran.cc_mitm_framework.card.R.array.compatible_aid_list).toList()
        aidStat = cardEmulation2.registerAidsForService(myName, "payment", dynamicAIDList)
        dynamicAIDList = cardEmulation2.getAidsForService(myName, "payment")
        Log.v("Dynamic AID", dynamicAIDList.toString())


    }

    //fun registerBroadcastListener() {

    //LocalBroadcastManager.getInstance(applicationContext).registerReceiver(messageReceiver, IntentFilter(CREDITCARACTIVITY_INTENT))
    //}

    override fun onStop() {
        super.onStop()
        //    unRegisterBroadcastListener()
    }

    override fun onStart() {
        super.onStart()
        // registerBroadcastListener()
    }

    //fun unRegisterBroadcastListener() {
    //  LocalBroadcastManager.getInstance(applicationContext).unregisterReceiver(messageReceiver);
    //}


    fun changeTapAndPay() {
        var intent = Intent()
        intent.action = CardEmulation.ACTION_CHANGE_DEFAULT
        intent.putExtra(CardEmulation.EXTRA_SERVICE_COMPONENT, ComponentName(super.getBaseContext(), CreditCardService::class.java))
        intent.putExtra(CardEmulation.EXTRA_CATEGORY, CardEmulation.CATEGORY_PAYMENT)
        startActivity(intent)
    }

    override fun handleTxMessage(msg: String?) {
        LoggingHelper.writeToFile(baseContext, "Handling a TX message received from the websocket\n $msg", LoggingHelper.LOG_TYPE_VERBOSE)
        var responseMsg = Message.MessageFromJson(msg ?: "")
        val strMessageCache: String = PreferencesHelper.getMessageCache(baseContext)
        if (CreditCardService.firstTime) {
            connectionsText = "$connectionsText <br/> ${getString(R.string.get_close_to_pos)}"
            runOnUiThread(object : Runnable {
                override fun run() {
                    connectionLogWv.loadData(connectionsText, "text/html; charset=utf-8", "utf-8")
                }
            })
        }
        PreferencesHelper.setMessageCache(baseContext, responseMsg.toJson())
        //var intent = Intent(CreditCardService.CREDITCARSERVICE_INTENT)
        //intent.putExtra("message", responseMsg.toJson())
        //intent.addFlags(Intent.FLAG_RECEIVER_FOREGROUND)
        //LocalBroadcastManager.getInstance(applicationContext).sendBroadcast(intent)
    }

    override fun handleAuthMessage(msg: String?) {
        connectionsText = "$connectionsText <br/> ${getString(R.string.remote_reader_can_begin)}"
        runOnUiThread(object : Runnable {
            override fun run() {
                connectionLogWv.loadData(connectionsText, "text/html; charset=utf-8", "utf-8")
            }
        })
    }

    /*class MessageFromReaderBroadcastReceiver : BroadcastReceiver {
        private var ctx: MainActivity

        constructor (ctx: MainActivity) : super() {

            this.ctx = ctx
        }

        override fun onReceive(p0: Context?, p1: Intent?) {
            LoggingHelper.writeToFile(ctx, "Coming to activity, message from service:\n"
            + p1?.extras?.getString("message")
                    ?: "", LoggingHelper.LOG_TYPE_VERBOSE)
            var m = Message.MessageFromJson(p1?.extras?.getString("message")?:"")
            ctx.websocketHelper?.sendMessage(m.toJson())

        }

    }*/

}
