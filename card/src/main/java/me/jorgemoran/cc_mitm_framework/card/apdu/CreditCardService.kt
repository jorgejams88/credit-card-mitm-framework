package me.jorgemoran.cc_mitm_framework.card.apdu

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.nfc.cardemulation.HostApduService
import android.os.Bundle
import android.support.v4.content.LocalBroadcastManager
import me.jorgemoran.cc_mitm_framework.card.MainActivity
import me.jorgemoran.cc_mitm_framework.common.apdu.MessagesHelper
import me.jorgemoran.cc_mitm_framework.common.entity.MESSAGE_ORIGIN
import me.jorgemoran.cc_mitm_framework.common.entity.MESSAGE_TYPE
import me.jorgemoran.cc_mitm_framework.common.entity.Message
import me.jorgemoran.cc_mitm_framework.common.logging.LoggingHelper
import me.jorgemoran.cc_mitm_framework.common.network.WebSocketHelper
import me.jorgemoran.cc_mitm_framework.common.persistence.PreferencesHelper


class CreditCardService : HostApduService() {

    companion object {
        val CREDITCARSERVICE_INTENT = "me.jorgemoran.cc_mitm_framework.card.message-from-socket"
        var responseFromSocket: Message? = null
        var firstTime = true
    }

    //private lateinit var messageReceiver: BroadcastReceiver


    override fun onDeactivated(p0: Int) {
        firstTime = true
    }

    override fun onCreate() {
        super.onCreate()
      //  messageReceiver = MessageToReaderBroadcastReceiver(this)
        //registerBroadcastListener()
    }

    override fun onDestroy() {
        super.onDestroy()
        firstTime = true
       // unRegisterBroadcastListener()
    }

    /*fun registerBroadcastListener() {

        LoggingHelper.writeToFile(baseContext, "[CardService] Registering broadcast listener", LoggingHelper.LOG_TYPE_VERBOSE)
        LocalBroadcastManager.getInstance(applicationContext).registerReceiver(messageReceiver, IntentFilter(CREDITCARSERVICE_INTENT))
    }

    fun unRegisterBroadcastListener() {
        LoggingHelper.writeToFile(baseContext, "[CardService] Unregistering broadcast listener", LoggingHelper.LOG_TYPE_VERBOSE)
        LocalBroadcastManager.getInstance(applicationContext).unregisterReceiver(
                messageReceiver);
    }*/

    override fun processCommandApdu(p0: ByteArray?, p1: Bundle?): ByteArray {
        val commandApduAsString = MessagesHelper.ByteArrayToHexString(p0);
        var response = ByteArray(1)
        LoggingHelper.writeToFile(baseContext, "Command:\n $commandApduAsString")
        var continueWaiting = true
        if (firstTime == true) {
            responseFromSocket = getWebsocketResponseFromFile()
            firstTime = false
        }
        if (responseFromSocket == null) {
            val t = Thread(Runnable
            {
                requestResponseForReaderMessage(commandApduAsString)
            }
            ).start()
        }
        val startingTime = System.currentTimeMillis()
        LoggingHelper.writeToFile(baseContext, "[CardService] Before starting to wait for response from websocket", LoggingHelper.LOG_TYPE_VERBOSE)
        CreditCardService.responseFromSocket = getWebsocketResponseFromFile()
        while (CreditCardService.responseFromSocket == null && continueWaiting) {
            try {
                Thread.sleep(5)
            } catch (e: Exception) {
                LoggingHelper.writeExceptionToErrorLog(baseContext, e)
            }
            if (!((System.currentTimeMillis() - startingTime) < 10000)) {
                continueWaiting = false
            }
            CreditCardService.responseFromSocket = getWebsocketResponseFromFile()
        }
        LoggingHelper.writeToFile(baseContext, "[CardService] Finished waiting for response from websocket", LoggingHelper.LOG_TYPE_VERBOSE)
        if (responseFromSocket == null) {
            //TODO handle communication error
            LoggingHelper.writeToFile(baseContext, "Time expired and response was not received", LoggingHelper.LOG_TYPE_VERBOSE)
            return ByteArray(2)
        } else {
            LoggingHelper.writeToFile(baseContext, "Received value from socket:\n" + responseFromSocket, LoggingHelper.LOG_TYPE_VERBOSE)
        }
        //TODO: Verify if the message gotten from the websocket actually contains the request that was made
        response = MessagesHelper.HexStringToByteArray(CreditCardService.responseFromSocket?.apduResponse)
        val responseStr = CreditCardService.responseFromSocket?.apduResponse
        responseFromSocket = null
        PreferencesHelper.clearMessageCache(baseContext)
        LoggingHelper.writeToFile(baseContext, "Response from card:\n $responseStr")
        return response;
    }

    fun requestResponseForReaderMessage(command: String) {
        LoggingHelper.writeToFile(baseContext, "Sending broadcast to request response for $command", LoggingHelper.LOG_TYPE_VERBOSE)
        var message = Message(MESSAGE_ORIGIN.CARD.type, MESSAGE_TYPE.TX.type, command)
        message.uuid = PreferencesHelper.getTransactionUUID(baseContext)
        //var intent = Intent(MainActivity.CREDITCARACTIVITY_INTENT)
        //intent.putExtra("message", message.toJson())
        //intent.addFlags(Intent.FLAG_RECEIVER_FOREGROUND)
        //LocalBroadcastManager.getInstance(applicationContext).sendBroadcast(intent)
        WebSocketHelper.currentWebsocket?.sendMessage(message.toJson())
    }

    fun getWebsocketResponseFromFile(): Message? {
        val strMsg = PreferencesHelper.getMessageCache(baseContext)
        if (strMsg == null || strMsg.isEmpty() || strMsg.isBlank() || strMsg.equals("")) {
            return null
        } else {
            return Message.MessageFromJson(strMsg)
        }
    }

/*    class MessageToReaderBroadcastReceiver : BroadcastReceiver {
        private var ctx: CreditCardService

        constructor (ctx: CreditCardService) : super() {

            this.ctx = ctx
        }

        override fun onReceive(p0: Context?, p1: Intent?) {
            LoggingHelper.writeToFile(ctx, "Returning to service, message from activity:\n"
            + p1?.extras?.getString("message") ?: "empty", LoggingHelper.LOG_TYPE_VERBOSE)
            val response = p1?.extras?.getString("message") ?: "empty"
            CreditCardService.responseFromSocket = Message.MessageFromJson(response)
        }
    }*/
}