package me.jorgemoran.cc_mitm_framework.reader

import android.content.Context
import android.os.Bundle
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*
import me.jorgemoran.cc_mitm_framework.reader.apdu.reader.CreditCardReader
import me.jorgemoran.cc_mitm_framework.common.activity.BaseActivity
import me.jorgemoran.cc_mitm_framework.common.apdu.MessagesHelper
import me.jorgemoran.cc_mitm_framework.common.entity.MESSAGE_ORIGIN
import me.jorgemoran.cc_mitm_framework.common.entity.Message
import me.jorgemoran.cc_mitm_framework.reader.apdu.reader.CardReaderListener


class MainActivity : BaseActivity(), CardReaderListener {

    var reader: CreditCardReader = CreditCardReader(this)
    override var currentBehavior: Int
        get() = MESSAGE_ORIGIN.READER.type
        set(value) {}

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(me.jorgemoran.cc_mitm_framework.common.R.layout.activity_main)
        listeningToggleButton = btnStart
        serverAddressEditText = editSrvAddr
        connectionLogWv = connectionsLog
    }

    public override fun onPause() {
        super.onPause()
        hwHelper.disableReaderMode()
    }

    public override fun onResume() {
        super.onResume()
        hwHelper.enableReader(reader)
    }

    public override fun onStop() {
        super.onStop()
        closeConnectionClearLog()
    }

    override fun handleTxMessage(msg: String?)
    {
        var responseMsg = Message.MessageFromJson(msg ?: "")
        reader.sendMessageToCard(MessagesHelper.HexStringToByteArray(responseMsg.apduRequest))

    }
    override fun handleAuthMessage(msg: String?){
        connectionsText = "$connectionsText <br/> ${getString(R.string.waiting_for_card)}"
        runOnUiThread(object : Runnable {
            override fun run() {
                connectionLogWv.loadData(connectionsText, "text/html; charset=utf-8", "utf-8")
            }
        })
    }


    override fun onCardReaderError(type: Int) {
        runOnUiThread(object : Runnable {
            override fun run() {
                Toast.makeText(this@MainActivity, type, Toast.LENGTH_LONG).show()
            }
        })
        resetActivity()
    }


    override fun isWebSocketActive(): Boolean {
        return listening
    }

    override fun getActivityContext(): Context {
        return baseContext
    }

    override fun getNextReaderRequest(message: Message) {
        websocketHelper?.sendMessage(message.toJson())
    }
}
