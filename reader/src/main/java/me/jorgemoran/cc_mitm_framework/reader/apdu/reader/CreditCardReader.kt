package me.jorgemoran.cc_mitm_framework.reader.apdu.reader

import android.content.Context
import android.nfc.NfcAdapter
import android.nfc.Tag
import android.nfc.TagLostException
import android.nfc.tech.IsoDep
import android.util.Log
import me.jorgemoran.cc_mitm_framework.common.apdu.MessagesHelper
import me.jorgemoran.cc_mitm_framework.common.entity.MESSAGE_ORIGIN
import me.jorgemoran.cc_mitm_framework.common.entity.MESSAGE_TYPE
import me.jorgemoran.cc_mitm_framework.common.entity.Message
import me.jorgemoran.cc_mitm_framework.common.logging.LoggingHelper
import me.jorgemoran.cc_mitm_framework.common.persistence.PreferencesHelper
import me.jorgemoran.cc_mitm_framework.reader.R

class CreditCardReader(val listener: CardReaderListener) : NfcAdapter.ReaderCallback {
    companion object {
        val TAG = "CARD_READER"
    }

    private lateinit var isoDep: IsoDep
    override fun onTagDiscovered(tag: Tag?) {
        if (!listener.isWebSocketActive()) {
            listener.onCardReaderError(R.string.websocket_not_connected)
            //return
        }

        isoDep = IsoDep.get(tag)
        if (isoDep != null) {
            isoDep.connect()
            sendAidMessage()
        }
    }

    fun sendAidMessage() {

        LoggingHelper.writeToFile(listener.getActivityContext(), "Requesting remote AID: ${MessagesHelper.PPSE_AID}", LoggingHelper.LOG_TYPE_VERBOSE)
        var command = MessagesHelper.BuildSelectApdu(MessagesHelper.PPSE_AID)
        sendMessageToCard(command)
        //val statusWord = statusWordFromMessage(result)
        //var payload = payloadFromMessage(result)

    }

    fun sendMessageToCard(message: ByteArray) {
        if (isoDep.isConnected) {
            try {
                var result = isoDep.transceive(message)
                Log.i(TAG, "Sending: " + MessagesHelper.ByteArrayToHexString(message))
                Log.i(TAG, "Sending: " + MessagesHelper.ByteArrayToHexString(message))
                LoggingHelper.writeToFile(listener.getActivityContext(), "Command:\n" + MessagesHelper.ByteArrayToHexString(message))
                LoggingHelper.writeToFile(listener.getActivityContext(), "Response:\n" + MessagesHelper.ByteArrayToHexString(result))
                val msg = Message(MESSAGE_ORIGIN.READER.type,MESSAGE_TYPE.TX.type)
                msg.uuid=PreferencesHelper.getTransactionUUID(listener.getActivityContext())
                msg.apduRequest = MessagesHelper.ByteArrayToHexString(message)
                msg.apduResponse = MessagesHelper.ByteArrayToHexString(result)
                listener.getNextReaderRequest(msg)
            } catch (e: TagLostException) {
                LoggingHelper.writeExceptionToErrorLog(listener.getActivityContext(), e)
                listener.onCardReaderError(R.string.reader_communication_error)

            }
        } else {
            listener.onCardReaderError(R.string.reader_communication_error)
        }
    }
}

interface CardReaderListener {
    fun onCardReaderError(type: Int)
    fun isWebSocketActive(): Boolean
    fun getActivityContext(): Context
    fun getNextReaderRequest(message: Message)
}